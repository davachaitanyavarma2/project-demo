# https://www.packer.io/docs/templates/hcl_templates/blocks/build

build {

  name                        = "aws"
  sources                     = [
                              "source.amazon-ebs.amzn2-arm",
                              "source.amazon-ebs.amzn2-x64",
                              ]

  provisioner "file" {
    destination               = "/tmp"
    source                    = "files"
  }

  provisioner "shell" {
    environment_vars          = [
                                "ADD_PAKS=${var.add_packages}",
                                "CROWDSTRIKE_URL=${var.amzn2_arm_crowdstrike}",
                                "AWS_CLI=${var.amzn2_arm_aws_zip}",
                                "TELEGRAF_URL=${var.amzn2_arm_telegraf}",
                                "FLUENTD_URL=${var.amzn2_arm_fluentd}",
                                "CUSTOM_USER=${var.custom_user}",
                                "CUSTOM_PUB_KEY=${var.custom_pub_key}",
                                "DEPLOY_USER=${var.deploy_user}",
                                "DEPLOY_PUB_KEY=${var.deploy_pub_key}",
                                "APP_USER=${var.app_user}", 
                              ]
    remote_folder             = "/home/ec2-user"
    only                      = ["amazon-ebs.amzn2-arm"]    
    scripts                   = fileset(".", "scripts/{amzn2-arm,setup,cleanup}.sh")
    pause_before              = "10s"
    pause_after               = "10s"    
  }

  provisioner "shell" {
    environment_vars          = [
                                "ADD_PAKS=${var.add_packages}",
                                "CROWDSTRIKE_URL=${var.amzn2_x64_crowdstrike}",
                                "AWS_CLI=${var.amzn2_x64_aws_zip}",
                                "TELEGRAF_URL=${var.amzn2_x64_telegraf}",
                                "FLUENTD_URL=${var.amzn2_x64_fluentd}",
                                "CUSTOM_USER=${var.custom_user}",
                                "CUSTOM_PUB_KEY=${var.custom_pub_key}",
                                "DEPLOY_USER=${var.deploy_user}",
                                "DEPLOY_PUB_KEY=${var.deploy_pub_key}",
                                "APP_USER=${var.app_user}",
                              ]
    remote_folder             = "/home/ec2-user"
    only                      = ["amazon-ebs.amzn2-x64"]    
    scripts                   = fileset(".", "scripts/{amzn2-x64,setup,cleanup}.sh")
    pause_before              = "10s"
    pause_after               = "10s"
  }  

}

